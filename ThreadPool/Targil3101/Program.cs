﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Targil3101
{
    class Program
    {
        static Random r = new Random();
        static void Main(string[] args)
        {
            ThreadSafeQueue threadSafeQueue = new ThreadSafeQueue();
            List<Thread> threads = new List<Thread>();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int j = 0; j < 100; j++)
            {
                ThreadPool.QueueUserWorkItem((o) =>
                {
                    for (int i = 0; i < 100; i++)
                    {
                        //threadSafeQueue.Push(i);
                        Thread.Sleep(r.Next(100, 500));
                    }
                });
                //threads.Add(new Thread(() =>
                //{
                //    for (int i = 0; i < 100; i++)
                //    {
                //        //threadSafeQueue.Push(i);
                //        Thread.Sleep(r.Next(100, 500));
                //    }
                //}));

                ThreadPool.QueueUserWorkItem((o) =>
                {
                    Thread.Sleep(500);
                    for (int i = 0; i < 50; i++)
                    {
                        // threadSafeQueue.Pop();
                        Thread.Sleep(r.Next(500, 1000));
                    }
                });
                //threads.Add(new Thread(() =>
                //{
                //    Thread.Sleep(500);
                //    for (int i = 0; i < 50; i++)
                //    {
                //       // threadSafeQueue.Pop();
                //        Thread.Sleep(r.Next(500, 1000));
                //    }
                //}));

                ThreadPool.QueueUserWorkItem((o) =>
                {
                    Thread.Sleep(500);
                    for (int i = 0; i < 5; i++)
                    {
                        //threadSafeQueue.Peep();
                        Thread.Sleep(r.Next(100, 200));
                    }
                });
                //threads.Add(new Thread(() =>
                //{
                //    for (int i = 0; i < 5; i++)
                //    {
                //        //threadSafeQueue.Peep();
                //        Thread.Sleep(r.Next(100, 200));
                //    }
                //}));
            }
            Console.WriteLine("Wait...");
            //Thread.Sleep(3000);
            //foreach (var item in threads)
            //{
            //item.Start();
            //}
            var time = sw.ElapsedMilliseconds;
            Debug.WriteLine(sw.ElapsedMilliseconds);
        }
    }
}
