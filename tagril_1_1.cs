using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Targil3101
{
    class Program
    {

        static void Main(string[] args)
        {
            Thread count1_10 = new Thread(() =>
            {
                for (int i = 1; i <= 10; i++)
                {
                    Console.WriteLine($"=== thread : {Thread.CurrentThread.ManagedThreadId} i: {i}");
                    Thread.Sleep(1 * 1000);
                }

            });
            count1_10.Start();
            
            Thread count11_20 = new Thread(() =>
            {
                count1_10.Join();
                for (int i = 11; i <= 20; i++)
                {
                    Console.WriteLine($"=== thread : {Thread.CurrentThread.ManagedThreadId} i: {i}");
                    Thread.Sleep(1 * 1000);
                }

            });
            count11_20.Start();

        }
    }
}
