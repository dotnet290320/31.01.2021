﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Threads3101
{
    class Program
    {
        static object key = new object();
        public static void Counter()
        {
            Console.WriteLine($"=== thread : {Thread.CurrentThread.ManagedThreadId} before lock ...");
            lock (key)
            {
                for (int i = 1; i <= 10; i++)
                {
                    Console.WriteLine($"=== thread : {Thread.CurrentThread.ManagedThreadId} i: {i}");
                    Thread.Sleep(1 * 1000);
                }
            }
        }
        static void Main(string[] args)
        {
            //new Thread(Counter).Start();
            //new Thread(Counter).Start();
            //new Thread(Counter).Start();
            //new Thread(Counter).Start();

            WorkerQueue wq = new WorkerQueue(10);
            wq.Produce(() =>
            {
                for (int i = 1; i <= 5; i++)
                {
                    Console.WriteLine($"=== i_1_5 {i}");
                    Thread.Sleep(1 * 1000);
                }
            });

            wq.Produce(() =>
            {
                for (int i = 1; i <= 3; i++)
                {
                    Console.WriteLine($"=== i_1_3 {i}");
                    Thread.Sleep(1 * 1000);
                }
            });

            wq.Produce(() =>
            {
                for (int i = 1; i <= 8; i++)
                {
                    Console.WriteLine($"=== i_1_8 {i}");
                    Thread.Sleep(1 * 1000);
                }
            });

            Console.WriteLine("Main waiting...");
            Thread.Sleep(100);
            Console.WriteLine("Main finished...");
        }
    }
}
